(function() {
   const header = document.querySelector('#header');

   function handleClick(e) {
      e.preventDefault();
      if (e.target.hash) {
         const topOffset = 200;
         const hash = e.target.hash;
         const elem = document.querySelector(hash);
         const topCords = elem.offsetTop;
         window.scroll({
            top: topCords - topOffset,
            behavior: 'smooth',
         });
      }
   }

   header.addEventListener('click', handleClick);
})();
