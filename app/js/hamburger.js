(function() {
   const header = document.querySelector('#header');
   const hamburger = document.querySelector('#hamburger');
   hamburger.addEventListener('click', toggleHeader);
   document.addEventListener('click', function(e) {
      if (
         header.className.includes('active') &&
         !e.target.hash &&
         !e.target.closest('#hamburger')
      ) {
         header.classList.remove('active');
         hamburger.classList.remove('is-active');
      }
   });

   function toggleHeader() {
      hamburger.classList.toggle('is-active');
      header.classList.toggle('active');
   }
})();
